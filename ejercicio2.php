<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="css/delimeters.css">
	<link rel="stylesheet" type="text/css" href="css/tabs.css">
	<title>ejercicio2</title>
</head>
<body>
	<section>
		<article>
			<div class="contenedor-tabs">
				<?php 

					echo "<span class=\"diana\" id=\"una\"></span>\n";
					echo "<div class=\"tab\">\n";
					echo "<a href=\"#una\" class=\"tab-e\">estilo XML</a>\n";
					echo "<div class=\"first\">\n";
					echo "<p class=\"xmltag\">";
					echo "este texto esta escrito en php, utilizando las etiquetas mas ";
					echo "usuales y recomendadas para delimitar el codigo php  que son ";
					echo "&lt;?php .... ?&gt;.<br>\n";
					echo "</p>\n";
					echo "</div>\n";
					echo "</div>";
				 ?>

				 <?php /* no me acepta la forma "<? ?>" */

				 	echo "<span class=\"diana\" id=\"tres\"></span>\n";
				 	echo "<div class=\"tab\">\n";
				 	echo "<a href=\"#tres\" class=\"tab-e\">etiquets cortas</a>\n";
				 	echo "<div>\n";
				 	echo "<p class=\"shorttag\">";
				 	echo "este texto  tambien est a escrito en php, utilizando las etiquetas ";
				 	echo "cortas, <br>\n que son: &lt;? ... ?&gt;";
				 	echo "</p>\n";
				 	echo "</div>\n";
				 	echo "</div>\n";

				 ?>
			</div>
		</article>
	</section>



</body>
</html>